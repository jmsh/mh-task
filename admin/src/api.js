const config = require("config")
const axios = require("axios")
const R = require("ramda")

/**
 * Get investment details for the provided investment id.
 * @param {string} id - the investment id.
 * @returns {Promise<string>} resolves to investment details as JSON string.
 */
function getInvestmentId(id) {
  return axios.get(`${config.investmentsServiceUrl}/${id}`).then((r) => {
    return r.data
  }).catch((e) => {
    // default axios error returned as HTML by Express default error handler,
    // could be customised and changed to JSON...
    throw e
  })
}

async function getAllHoldingsReport() {
  // all investments and all holding names
  const promises = [axios.get(config.investmentsServiceUrl), axios.get(config.financialCompaniesServiceUrl)]
  const readResponses = (responses) => { // learning Ramda basics...
    const getData = response => response.data
    return R.pipe(
      R.map(getData),
      R.zipObj(["investments", "companies"]),
    )(responses)
  }
  const {investments, companies} = await Promise.all(promises)
    .then(readResponses)
    .catch((e) => {
      // throw more readable message
      throw e
    })
    // TODO Ramdarise - not ready to use for this yet, more time / familiarity needed with func prog
    // A combination of functions needed utilising R.map, R.find, R.propEq probably...
  const report = []
  for (const investment of investments) {
    for (const { id, investmentPercentage: pc } of investment.holdings) {
      const { name } = companies.find((company) => id === company.id)
      report.push({
        user: investment.userId,
        firstName: investment.firstName,
        lastName: investment.lastName,
        date: investment.date,
        holding: name,
        value: investment.investmentTotal * pc,
      })
    }
  }
  return report
}

module.exports = function Api({ }) { // nothing needed currently
  return {
    getInvestmentId,
    getAllHoldingsReport,
  }
}
