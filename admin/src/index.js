const express = require("express")
const bodyParser = require("body-parser")
const config = require("config")
const Api = require("./api")
const app = express()
const api = new Api({}) // inject dependencies as needed
const {Parser} = require("json2csv")
const {default: axios} = require("axios")

app.use(bodyParser.json({limit: "10mb"}))

app.get("/investments/:id", (req, res, next) => {
  const {id} = req.params
  Promise.resolve()
    .then(() => api.getInvestmentId(id))
    .then((json) => res.json(json))
    .catch(next)
})

app.get("/reports/all-holdings", (req, res, next) => {
  Promise.resolve()
    .then(() => api.getAllHoldingsReport())
    .then(async (report) => {
      // TODO move this to e.g. /components/csvGenerator for re-usability and tests, but due to time...
      const fields = [
        {label: "User", value: "user"},
        {label: "First Name", value: "firstName"},
        {label: "Last Name", value: "lastName"},
        {label: "Date", value: "date"},
        {label: "Holding", value: "holding"},
        {label: "Value", value: "value"},
      ]
      const parser = new Parser({fields})
      const csv = parser.parse(report)
      // TODO - move this to e.g. /components/reportSender with seperate tests
      // investments body parser is JSON only so doesn't log csv
      await axios.post(`${config.investmentsServiceUrl}/export`, {
        csv,
      }, {
        headers: {"Content-Type": "application/json"},
      })
      return report
    })
    .then((report) => {
      res.json({
        message: "Report generated and sent to the investments service successfully.",
        report, // don't need this but makes checking easier
      })
    })
    .catch(next)
})

app.listen(config.port, (err) => {
  if (err) {
    console.error("Error occurred starting the server", err)
    process.exit(1)
  }
  console.log(`Server running on port ${config.port}`)
})
