const axios = require("axios")
const api = require("../api")({})

jest.mock("axios")
axios.get = jest.fn()

describe("Test getInvestmentId()", () => {
  it("should get investment details", async () => {
    const investmentId1 = [
      {
        "id": "1",
        "userId": "1",
        "firstName": "Billy",
        "lastName": "Bob",
        "investmentTotal": 1400,
        "date": "2020-01-01",
        "holdings": [
          {
            "id": "2",
            "investmentPercentage": 1,
          },
        ],
      },
    ]
    const axiosResp = {
      data: investmentId1,
    }
    axios.get.mockResolvedValueOnce(axiosResp)
    const resp = await api.getInvestmentId(1)
    expect(resp).toBe(investmentId1)
  })

  it("should throw error", async () => {
    const error = new Error("Error: Request failed with status code 404")
    axios.get.mockRejectedValueOnce(error)
    await expect(api.getInvestmentId(1)).rejects.toThrow(error)
  })
})

describe("Test getAllHoldingsReport()", () => {
  it("should fail as not written yet", () => {
    expect(true).toBe(false)
  })
})
